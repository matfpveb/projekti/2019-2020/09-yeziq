import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-loading-circle',
  templateUrl: './loading-circle.component.html',
  styleUrls: ['./loading-circle.component.css']
})
export class LoadingCircleComponent implements OnInit {

  @Input()
  public desc: string;

  constructor() { }

  ngOnInit() {
  }

}
