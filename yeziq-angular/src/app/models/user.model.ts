export interface User {
    _id: string,
    username: string,
    password: string,
    email: string
    srcLang: string,
    targetLang: string
};