export interface Word {
    word: string,
    status: number,
    chosenTrans: string,
    possibleTrans: Array<string>,
    userTrans: string
};