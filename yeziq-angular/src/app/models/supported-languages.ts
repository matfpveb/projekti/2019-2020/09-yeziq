/**
 *  Kolekcije vezane za podrzane jezike, njihove oznake, nazive,
 *  regularne izraze, putanje do slicica zastava.
 */

// Informativne liste sa oznakama podrzanih izvornih i ciljnih jezika.
const SrcLanguages = ['en', 'sr', 'de', 'fr'];
const TargetLanguages = ['en', 'de', 'fr', 'es', 'it', 'sr', 'pt', 'el', 'ru'];

// Mapa sa relativnim putanjama zastavica (iz prvih podfoldera foldera app)
const FlagLinks = new Map<string, string>();
FlagLinks.set('en', '../assets/country flags/united-kingdom-flag-icon-32.png');
FlagLinks.set('de', '../assets/country flags/germany-flag-icon-32.png');
FlagLinks.set('fr', '../assets/country flags/france-flag-icon-32.png');
FlagLinks.set('es', '../assets/country flags/spain-flag-icon-32.png');
FlagLinks.set('it', '../assets/country flags/italy-flag-icon-32.png');
FlagLinks.set('sr', '../assets/country flags/serbia-flag-icon-32.png');
FlagLinks.set('pt', '../assets/country flags/portugal-flag-icon-32.png');
FlagLinks.set('el', '../assets/country flags/greece-flag-icon-32.png');
FlagLinks.set('ru', '../assets/country flags/russia-flag-icon-32.png');


const NavbarFlagLinks = new Map<string, string>();
NavbarFlagLinks.set('en', '../assets/country flags/united-kingdom-flag-icon-35.png');
NavbarFlagLinks.set('de', '../assets/country flags/germany-flag-icon-35.png');
NavbarFlagLinks.set('fr', '../assets/country flags/france-flag-icon-35.png');
NavbarFlagLinks.set('es', '../assets/country flags/spain-flag-icon-35.png');
NavbarFlagLinks.set('it', '../assets/country flags/italy-flag-icon-35.png');
NavbarFlagLinks.set('sr', '../assets/country flags/serbia-flag-icon-35.png');
NavbarFlagLinks.set('pt', '../assets/country flags/portugal-flag-icon-35.png');
NavbarFlagLinks.set('el', '../assets/country flags/greece-flag-icon-35.png');
NavbarFlagLinks.set('ru', '../assets/country flags/russia-flag-icon-35.png');

// Mapa sa nazivima jezika u izvornim jezicima
const LangNames = new Map<string, Map<string, string>>();
LangNames.set('en', new Map<string, string>());
LangNames.set('de', new Map<string, string>());
LangNames.set('fr', new Map<string, string>());
LangNames.set('sr', new Map<string, string>());

// Engleski
LangNames.get('en').set('en', 'English');
LangNames.get('de').set('en', 'Englisch');
LangNames.get('fr').set('en', 'Anglais');
LangNames.get('sr').set('en', 'Енглески');
// Nemacki
LangNames.get('en').set('de', 'German');
LangNames.get('de').set('de', 'Deutsch');
LangNames.get('fr').set('de', 'Allemand');
LangNames.get('sr').set('de', 'Немачки');
// Francuski
LangNames.get('en').set('fr', 'French');
LangNames.get('de').set('fr', 'Französisch');
LangNames.get('fr').set('fr', 'Français');
LangNames.get('sr').set('fr', 'Француски');
// Spanski
LangNames.get('en').set('es', 'Spanish');
LangNames.get('de').set('es', 'Spanisch');
LangNames.get('fr').set('es', 'Espagnol');
LangNames.get('sr').set('es', 'Шпански');
// Italijanski
LangNames.get('en').set('it', 'Italian');
LangNames.get('de').set('it', 'Italienisch');
LangNames.get('fr').set('it', 'Italien');
LangNames.get('sr').set('it', 'Италијански');
// Srpski
LangNames.get('en').set('sr', 'Serbian');
LangNames.get('de').set('sr', 'Serbisch');
LangNames.get('fr').set('sr', 'Serbe');
LangNames.get('sr').set('sr', 'Српски');
// Portugalski
LangNames.get('en').set('pt', 'Portuguese');
LangNames.get('de').set('pt', 'Portugiesisch');
LangNames.get('fr').set('pt', 'Portugais');
LangNames.get('sr').set('pt', 'Португалски');
// Grcki
LangNames.get('en').set('el', 'Greek');
LangNames.get('de').set('el', 'Griechisch');
LangNames.get('fr').set('el', 'Grec');
LangNames.get('sr').set('el', 'Грчки');
// Ruski
LangNames.get('en').set('ru', 'Russian');
LangNames.get('de').set('ru', 'Russisch');
LangNames.get('fr').set('ru', 'Russe');
LangNames.get('sr').set('ru', 'Руски');

// Mapa sa regularnim izrazima za ciljne jezike
const RegexMap = new Map<string, string>();
RegexMap.set('en', '[a-zA-Z]+');
RegexMap.set('de', '[a-zA-ZäöüÄÖÜß]+');
RegexMap.set('fr', '[a-zA-ZàâäôéèëêïîçùûüÿæœÀÂÄÔÉÈËÊÏÎŸÇÙÛÜÆŒ]+');
RegexMap.set('es', '[a-zA-ZáéíñóúüÁÉÍÑÓÚÜ]+');
RegexMap.set('it', '[a-zA-ZàèéìíîòóùúÀÈÉÌÍÎÒÓÙÚ]+');
RegexMap.set('sr', '[абвгдђежзијклљмнњопрстћуфхцчџшАБВГДЂЕЖЗИЈКЛЉМНЊОПРСТЋУФХЦЧЏШ]+'); // Yandex podrzava samo cirilicu
RegexMap.set('pt', '[a-zA-ZãáàâçéêíõóôúüÃÁÀÂÇÉÊÍÕÓÔÚÜ]+');
RegexMap.set('el', '[α-ωΑ-ΩίϊΐόάέύϋΰήώΊΪΌΆΈΎΫΉΏ]+');
RegexMap.set('ru', '[абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ]+');

// Nazivi recnika i jezici koje podrzavaju u zahtevima
const DictSupportedLangMap = new Map<string, string[]>();
DictSupportedLangMap.set('Google Translate', ['en', 'de', 'fr', 'sr', 'es', 'it', 'pt', 'el', 'ru']);
DictSupportedLangMap.set('WordReference', ['en', 'de', 'fr', 'es', 'it', 'pt', 'ru']);
DictSupportedLangMap.set('Yandex', ['en', 'de', 'fr', 'sr', 'es', 'it', 'pt', 'el', 'ru']);
DictSupportedLangMap.set('Bing', ['en', 'de', 'fr', 'es', 'it', 'pt', 'el', 'ru']);

// Nazivi recnika i oblici zahteva. Podniske <srcLang>, <targetLang>, <word> se menjaju odgovarajucim vrednostima.
const DictionaryQueriesMap = new Map<string, string>();
DictionaryQueriesMap.set('Google Translate', 'https://translate.google.com/#view=home&op=translate&sl=<targetLang>&tl=<srcLang>&text=<word>');
DictionaryQueriesMap.set('WordReference', 'https://www.wordreference.com/<targetLang><srcLang>/<word>');
DictionaryQueriesMap.set('Yandex', 'https://translate.yandex.com/?lang=<targetLang>-<srcLang>&text=<word>');
DictionaryQueriesMap.set('Bing', 'https://www.bing.com/translator?from=<targetLang>&to=<srcLang>&text=<word>');

export { SrcLanguages, TargetLanguages, FlagLinks, NavbarFlagLinks, LangNames, RegexMap, DictSupportedLangMap, DictionaryQueriesMap };