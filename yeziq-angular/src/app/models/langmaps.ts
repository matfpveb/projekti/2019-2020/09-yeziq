/**
 *  Mape za natpise, naslove, tekstove dugmica itd. u podrzanim izvornim jezicima,
 *  Po jedna za Angular komponentu kojoj je potrebna.
 *
 *  Kljuc je skracenica za jezik.
 *  Vrednost je mapa ciji je kljuc interni naziv natpisa, dugmeta itd
 *  dok vrednost cini odgovarajuci tekst prikazan u odabranom jeziku.
 */

// LOGIN-PAGE-COMPONENT
const LoginPageMap = new Map<string, Map<string, string>>();
LoginPageMap.set('en', new Map<string, string>());
LoginPageMap.set('de', new Map<string, string>());
LoginPageMap.set('fr', new Map<string, string>());
LoginPageMap.set('sr', new Map<string, string>());

// Dugme za toggle formulara za registraciju
LoginPageMap.get('en').set('registerFormToggle', `I don't have an account.`);
LoginPageMap.get('de').set('registerFormToggle', `Ich habe keinen Account.`);
LoginPageMap.get('fr').set('registerFormToggle', `Je n'ai pas de compte.`);
LoginPageMap.get('sr').set('registerFormToggle', `Немам налог.`);

// Dugme za toggle formulara za login
LoginPageMap.get('en').set('loginFormToggle', 'I already have an account.');
LoginPageMap.get('de').set('loginFormToggle', 'Ich habe bereits einen Account.');
LoginPageMap.get('fr').set('loginFormToggle', `Je ai déjà un compte.`);
LoginPageMap.get('sr').set('loginFormToggle', `Већ имам налог.`);

// Naslov za login formular
LoginPageMap.get('en').set('loginFormHeading', 'Login Form');
LoginPageMap.get('de').set('loginFormHeading', 'Login Formular');
LoginPageMap.get('fr').set('loginFormHeading', 'Formulaire de connexion');
LoginPageMap.get('sr').set('loginFormHeading', 'Пријава');

// Username labela
LoginPageMap.get('en').set('usernameLabel', 'Username');
LoginPageMap.get('de').set('usernameLabel', 'Benutzername');
LoginPageMap.get('fr').set('usernameLabel', `Nom d'utilisateur`);
LoginPageMap.get('sr').set('usernameLabel', `Корисничко име`);

// Password labela
LoginPageMap.get('en').set('passwordLabel', 'Password');
LoginPageMap.get('de').set('passwordLabel', 'Passwort');
LoginPageMap.get('fr').set('passwordLabel', 'Mot de passe');
LoginPageMap.get('sr').set('passwordLabel', 'Лозинка');

// Confirm password labela
LoginPageMap.get('en').set('confirmPasswordLabel', 'Confirm password');
LoginPageMap.get('de').set('confirmPasswordLabel', 'Bestätigen Sie das Passwort');
LoginPageMap.get('fr').set('confirmPasswordLabel', 'Confirmez le mot de passe');
LoginPageMap.get('sr').set('confirmPasswordLabel', 'Потврди лозинку');

// Submit dugme za login formular
LoginPageMap.get('en').set('loginButton', 'Sign-in');
LoginPageMap.get('de').set('loginButton', 'Einloggen');
LoginPageMap.get('fr').set('loginButton', 'Connecter');
LoginPageMap.get('sr').set('loginButton', 'Пријави се');

// Naslov za formular registracije
LoginPageMap.get('en').set('registerFormHeading', 'Register Form');
LoginPageMap.get('de').set('registerFormHeading', 'Anmeldeformular');
LoginPageMap.get('fr').set('registerFormHeading', `Formulaire d'inscription`);
LoginPageMap.get('sr').set('registerFormHeading', `Регистрација`);

// Email labela
LoginPageMap.get('en').set('emailLabel', 'Email');
LoginPageMap.get('de').set('emailLabel', 'Email');
LoginPageMap.get('fr').set('emailLabel', 'Email');
LoginPageMap.get('sr').set('emailLabel', 'Електронска пошта');

// Labela za ciljni jezik
LoginPageMap.get('en').set('targetLangLabel', 'Learn');
LoginPageMap.get('de').set('targetLangLabel', 'Lernen');
LoginPageMap.get('fr').set('targetLangLabel', 'Apprendre');
LoginPageMap.get('sr').set('targetLangLabel', 'Учим');

// Submit dugme za formular registracije
LoginPageMap.get('en').set('registerButton', 'Register');
LoginPageMap.get('de').set('registerButton', 'Registrieren');
LoginPageMap.get('fr').set('registerButton', 'Enregistrer');
LoginPageMap.get('sr').set('registerButton', 'Креирај налог');

// Ispisi kod gresaka pri ispunjavanju login/register formulara
LoginPageMap.get('en').set('missingField', 'Missing field.');
LoginPageMap.get('de').set('missingField', 'Fehlendes Feld.');
LoginPageMap.get('fr').set('missingField', 'Champ manquant.');
LoginPageMap.get('sr').set('missingField', 'Поље је празно.');

LoginPageMap.get('en').set('incorrectUserPass', 'Incorrect username or password.');
LoginPageMap.get('de').set('incorrectUserPass', 'Benutzername oder Passwort falsch.');
LoginPageMap.get('fr').set('incorrectUserPass', `Nom d'utilisateur ou mot de passe incorrect.`);
LoginPageMap.get('sr').set('incorrectUserPass', 'Погрешно корисничко име или лозинка.');

LoginPageMap.get('en').set('alreadyExists', 'User already exists.');
LoginPageMap.get('de').set('alreadyExists', 'Benutzer existiert bereits.');
LoginPageMap.get('fr').set('alreadyExists', `L'utilisateur existe déjà.`);
LoginPageMap.get('sr').set('alreadyExists', 'Корисник већ постоји.');

LoginPageMap.get('en').set('usernameLength', 'Must be between 4 and 25 characters long.');
LoginPageMap.get('de').set('usernameLength', 'Muss zwischen 4 und 25 Zeichen lang sein.');
LoginPageMap.get('fr').set('usernameLength', 'Doit contenir entre 4 et 25 caractères.');
LoginPageMap.get('sr').set('usernameLength', 'Мора садржати од 4 до 25 карактера.');

LoginPageMap.get('en').set('passwordLength', 'Must be between 6 and 25 characters long.');
LoginPageMap.get('de').set('passwordLength', 'Muss zwischen 6 und 25 Zeichen lang sein.');
LoginPageMap.get('fr').set('passwordLength', 'Doit contenir entre 6 et 25 caractères.');
LoginPageMap.get('sr').set('passwordLength', 'Мора садржати од 6 до 25 карактера.');

LoginPageMap.get('en').set('unconfirmedPassword', 'Passwords must match.');
LoginPageMap.get('de').set('unconfirmedPassword', 'Passwörter müssen übereinstimmen.');
LoginPageMap.get('fr').set('unconfirmedPassword', 'Les mots de passe doivent correspondre.');
LoginPageMap.get('sr').set('unconfirmedPassword', 'Лозинке се морају поклапати.');

LoginPageMap.get('en').set('invalidEmail', 'Invalid email format.');
LoginPageMap.get('de').set('invalidEmail', 'Ungültiges Email-Format.');
LoginPageMap.get('fr').set('invalidEmail', `Format d'email invalide.`);
LoginPageMap.get('sr').set('invalidEmail', 'Неправилан облик електронске поште.');

// COURSES-PAGE-COMPONENT, HEADER-COMPONENT
const CoursesPageMap = new Map<string, Map<string, string>>();
CoursesPageMap.set('en', new Map<string, string>());
CoursesPageMap.set('de', new Map<string, string>());
CoursesPageMap.set('fr', new Map<string, string>());
CoursesPageMap.set('sr', new Map<string, string>());

// Naslov dobrodoslice
CoursesPageMap.get('en').set('welcomeHeading', 'Welcome');
CoursesPageMap.get('de').set('welcomeHeading', 'Willkommen');
CoursesPageMap.get('fr').set('welcomeHeading', 'Bienvenu/e');
CoursesPageMap.get('sr').set('welcomeHeading', 'Добродошли');

// Dugme za povratak na listu kurseva
CoursesPageMap.get('en').set('backToCoursesButton', 'Back to Courses');
CoursesPageMap.get('de').set('backToCoursesButton', 'Zurück zu Lehrgängen');
CoursesPageMap.get('fr').set('backToCoursesButton', 'Retour aux cours');
CoursesPageMap.get('sr').set('backToCoursesButton', 'Повратак на курсеве');

// Dugme za stranu za dodavanje kurseva
CoursesPageMap.get('en').set('addCourseButton', 'Add a course');
CoursesPageMap.get('de').set('addCourseButton', 'Lehrgang hinzufügen');
CoursesPageMap.get('fr').set('addCourseButton', 'Ajouter un cours');
CoursesPageMap.get('sr').set('addCourseButton', 'Додај курс');

// Naslov za Add course formular
CoursesPageMap.get('en').set('addCourseFormHeading', 'Add course');
CoursesPageMap.get('de').set('addCourseFormHeading', 'Lehrgang hinzufügen');
CoursesPageMap.get('fr').set('addCourseFormHeading', 'Ajouter un cours');
CoursesPageMap.get('sr').set('addCourseFormHeading', 'Додај курс');

// Course name labela
CoursesPageMap.get('en').set('courseNameLabel', 'Course name:');
CoursesPageMap.get('de').set('courseNameLabel', 'Lehrgangname:');
CoursesPageMap.get('fr').set('courseNameLabel', 'Nom du cours:');
CoursesPageMap.get('sr').set('courseNameLabel', 'Назив курса:');

// Tekst koji govori o biranju fajla ili unosenju teksta
CoursesPageMap.get('en').set('fileOrEnterText', 'You can import a file or enter text below:');
CoursesPageMap.get('de').set('fileOrEnterText', 'Sie können eine Datei importieren oder unten Text eingeben:');
CoursesPageMap.get('fr').set('fileOrEnterText', 'Vous pouvez importer un fichier ou saisir du texte ci-dessous:');
CoursesPageMap.get('sr').set('fileOrEnterText', 'Можете додати датотеку или унети текст испод:');

// Dugme za upload fajla
CoursesPageMap.get('en').set('browseButton', 'Browse files');
CoursesPageMap.get('de').set('browseButton', 'Dateien durchsuchen');
CoursesPageMap.get('fr').set('browseButton', 'Parcourir les fichiers');
CoursesPageMap.get('sr').set('browseButton', 'Прегледај датотеке');

// Enter text labela
CoursesPageMap.get('en').set('enterTextLabel', 'Enter text');
CoursesPageMap.get('de').set('enterTextLabel', 'Text eingeben');
CoursesPageMap.get('fr').set('enterTextLabel', 'Entrez du texte');
CoursesPageMap.get('sr').set('enterTextLabel', 'Унеси текст');

// Submit dugme za Add course formular
CoursesPageMap.get('en').set('createCourseButton', 'Create course');
CoursesPageMap.get('de').set('createCourseButton', 'Lehrgang erstellen');
CoursesPageMap.get('fr').set('createCourseButton', 'Créer un cours');
CoursesPageMap.get('sr').set('createCourseButton', 'Креирај курс');

// Ispis greske pri pravljenju kursa na osnovu fajla
CoursesPageMap.get('en').set('addCourseErrMsg', 'There was a problem with your file.');
CoursesPageMap.get('de').set('addCourseErrMsg', 'Es ist ein Problem mit Ihrer Datei aufgetreten.');
CoursesPageMap.get('fr').set('addCourseErrMsg', 'Un problème est survenu avec votre fichier.');
CoursesPageMap.get('sr').set('addCourseErrMsg', 'Дошло је до проблема са Вашом датотеком.');

// Predugacko ime za kurs
CoursesPageMap.get('en').set('addCourseFormNameTooLong', 'The course name is too long (30 characters maximum).')
CoursesPageMap.get('de').set('addCourseFormNameTooLong', 'Der Name des Lehrgangs ist zu lang (maximal 30 Zeichen).')
CoursesPageMap.get('fr').set('addCourseFormNameTooLong', 'Le nom du cours est trop long (30 caractères maximum).')
CoursesPageMap.get('sr').set('addCourseFormNameTooLong', 'Име курса је предугачко (максимум је 30 карактера).')

// Neispravna vrednost formulara za dodavanje kurseva
CoursesPageMap.get('en').set('addCourseFormErrMsg', 'Please enter course name and text or select a file.');
CoursesPageMap.get('de').set('addCourseFormErrMsg', 'Bitte geben Sie den Kursnamen und den Text ein oder wählen Sie eine Datei aus.');
CoursesPageMap.get('fr').set('addCourseFormErrMsg', `Veuillez saisir le nom et le texte du cours ou sélectionner un fichier.`);
CoursesPageMap.get('sr').set('addCourseFormErrMsg', 'Унесите назив и текст курса или одаберите датотеку.');

// Problem kod unosenja kursa u BP
CoursesPageMap.get('en').set('addCourseSaveErrMsg', 'There was a problem with saving your course.');
CoursesPageMap.get('de').set('addCourseSaveErrMsg', 'Beim Speichern Ihres Lehrgangs ist ein Problem aufgetreten.');
CoursesPageMap.get('fr').set('addCourseSaveErrMsg', `Un problème est survenu lors de l'enregistrement de votre cours.`);
CoursesPageMap.get('sr').set('addCourseSaveErrMsg', 'Дошло је до проблема са похрањивањем курса.');

// Courses natpis/naslov na delu See existing, link u navbaru
CoursesPageMap.get('en').set('coursesHeading', 'Courses');
CoursesPageMap.get('de').set('coursesHeading', 'Lehrgänge');
CoursesPageMap.get('fr').set('coursesHeading', 'Cours');
CoursesPageMap.get('sr').set('coursesHeading', 'Курсеви');

// Link za logout
CoursesPageMap.get('en').set('logoutLink', 'Logout');
CoursesPageMap.get('de').set('logoutLink', 'Ausloggen');
CoursesPageMap.get('fr').set('logoutLink', 'Déconnexion');
CoursesPageMap.get('sr').set('logoutLink', 'Одјава');

// Tekst u slucaju da korisnik nema nijedan kurs
CoursesPageMap.get('en').set('noCoursesText', `You haven't added any courses.`);
CoursesPageMap.get('de').set('noCoursesText', `Sie haben keine Lehrgänge hinzugefügt.`);
CoursesPageMap.get('fr').set('noCoursesText', `Vous n'avez ajouté aucun cours.`);
CoursesPageMap.get('sr').set('noCoursesText', `Нисте креирали ниједан курс.`);

// Course natpis pored imena izabranog kursa
CoursesPageMap.get('en').set('courseHeading', 'Course');
CoursesPageMap.get('de').set('courseHeading', 'Lehrgang');
CoursesPageMap.get('fr').set('courseHeading', 'Cours');
CoursesPageMap.get('sr').set('courseHeading', 'Курс');

// Hover nad slicicom za brisanje kursa i opcija za potvrdu brisanja
CoursesPageMap.get('en').set('removeCourseTitle', 'Remove');
CoursesPageMap.get('de').set('removeCourseTitle', 'Entfernen');
CoursesPageMap.get('fr').set('removeCourseTitle', 'Supprimer');
CoursesPageMap.get('sr').set('removeCourseTitle', 'Избриши');

// Cancel opcija koja ide uz opciju za potvrdu brisanja
CoursesPageMap.get('en').set('cancelSpan', 'Cancel');
CoursesPageMap.get('de').set('cancelSpan', 'Abbrechen');
CoursesPageMap.get('fr').set('cancelSpan', 'Annuler');
CoursesPageMap.get('sr').set('cancelSpan', 'Прекини');

// Ispis o procentu nepoznatih reci ispod imena kursa u listi kurseva (desni deo niske bez procenta)
CoursesPageMap.get('en').set('unknownPctByCourse', 'are unknown words');
CoursesPageMap.get('de').set('unknownPctByCourse', 'sind unbekannte Wörter');
CoursesPageMap.get('fr').set('unknownPctByCourse', 'sont des mots inconnus');
CoursesPageMap.get('sr').set('unknownPctByCourse', 'су непознате речи');

// Ispis ako nema nepoznatih reci za taj kurs ispod njegovog imena u listi kurseva
CoursesPageMap.get('en').set('allWordsKnownByCourse', 'You know all the words in this course!');
CoursesPageMap.get('de').set('allWordsKnownByCourse', 'Sie kennen alle Wörter in diesem Lehrgang!');
CoursesPageMap.get('fr').set('allWordsKnownByCourse', 'Vous connaissez tous les mots de ce cours!');
CoursesPageMap.get('sr').set('allWordsKnownByCourse', 'Знате све речи у овом курсу!');

// Lession ispis za listu lekcija
CoursesPageMap.get('en').set('lessonName', 'Lesson');
CoursesPageMap.get('de').set('lessonName', 'Lektion');
CoursesPageMap.get('fr').set('lessonName', 'Leçon');
CoursesPageMap.get('sr').set('lessonName', 'Лекција');

// Ispis za animaciju dok se dohvataju kursevi
CoursesPageMap.get('en').set('loadingCoursesDesc', 'Loading courses');
CoursesPageMap.get('de').set('loadingCoursesDesc', 'Lehrgänge werden geladen');
CoursesPageMap.get('fr').set('loadingCoursesDesc', 'Chargement des cours');
CoursesPageMap.get('sr').set('loadingCoursesDesc', 'Учитавам курсеве');

// Ispis za animaciju dok se kreira kurs
CoursesPageMap.get('en').set('creatingCourseDesc', 'Creating course');
CoursesPageMap.get('de').set('creatingCourseDesc', 'Kurs wird erstellt');
CoursesPageMap.get('fr').set('creatingCourseDesc', 'Création du cours');
CoursesPageMap.get('sr').set('creatingCourseDesc', 'Креирам курс');

// Ispis za animaciju dok se kreira kurs (dok traje cuvanje u bazi)
CoursesPageMap.get('en').set('savingCourseDesc', 'Saving course');
CoursesPageMap.get('de').set('savingCourseDesc', 'Kurs wird gespeichert');
CoursesPageMap.get('fr').set('savingCourseDesc', 'Enregistrement du course');
CoursesPageMap.get('sr').set('savingCourseDesc', 'Похрањујем курс');

// Natpis za level u navbaru
CoursesPageMap.get('en').set('levelName', 'Level');
CoursesPageMap.get('de').set('levelName', 'Stufe');
CoursesPageMap.get('fr').set('levelName', 'Niveau');
CoursesPageMap.get('sr').set('levelName', 'Ниво');

// Deo niske za level tooltip (navbar)
CoursesPageMap.get('en').set('levelTooltip', 'words until Level');
CoursesPageMap.get('de').set('levelTooltip', 'Wörter bis Stufe');
CoursesPageMap.get('fr').set('levelTooltip', `mots jusqu'au Niveau`);
CoursesPageMap.get('sr').set('levelTooltip', 'речи до Нивоа');

// 1. deo niske za tooltip poznate reci (navbar)
CoursesPageMap.get('en').set('knownWordsTooltip1', 'You currently know');
CoursesPageMap.get('de').set('knownWordsTooltip1', 'Sie kennen derzeit');
CoursesPageMap.get('fr').set('knownWordsTooltip1', 'Vous connaissez actuellement');
CoursesPageMap.get('sr').set('knownWordsTooltip1', 'Тренутно знате');

// 2. deo niske za tooltip poznate reci (navbar)
CoursesPageMap.get('en').set('knownWordsTooltip2', 'words');
CoursesPageMap.get('de').set('knownWordsTooltip2', 'Wörter');
CoursesPageMap.get('fr').set('knownWordsTooltip2', 'mots');
CoursesPageMap.get('sr').set('knownWordsTooltip2', 'речи');

CoursesPageMap.get('en').set('unknownWord', 'Unknown word');
CoursesPageMap.get('de').set('unknownWord', 'Unbekanntes Wort');
CoursesPageMap.get('fr').set('unknownWord', 'Mot inconnu');
CoursesPageMap.get('sr').set('unknownWord', 'Непозната реч');

CoursesPageMap.get('en').set('chosenTranslation', 'Chosen Translation');
CoursesPageMap.get('de').set('chosenTranslation', 'Ausgewählte Übersetzung');
CoursesPageMap.get('fr').set('chosenTranslation', 'Traduction Sélectionnée');
CoursesPageMap.get('sr').set('chosenTranslation', 'Изабрани превод');

CoursesPageMap.get('en').set('dictionaryTranslation', 'Check out the dictionary translation');
CoursesPageMap.get('de').set('dictionaryTranslation', 'Schauen Sie sich die Wörterbuchübersetzung an');
CoursesPageMap.get('fr').set('dictionaryTranslation', 'Découvrez la traduction du dictionnaire');
CoursesPageMap.get('sr').set('dictionaryTranslation', 'Погледајте превод речника');


// READING-SPACE-COMPONENT
const ReadingSpaceMap = new Map<string, Map<string, string>>();
ReadingSpaceMap.set('en', new Map<string, string>());
ReadingSpaceMap.set('de', new Map<string, string>());
ReadingSpaceMap.set('fr', new Map<string, string>());
ReadingSpaceMap.set('sr', new Map<string, string>());

// Natpis u sidebaru kada nije izabrana nijedna rec
ReadingSpaceMap.get('en').set('sidebarDesc', 'Click on a word to show its meaning');
ReadingSpaceMap.get('de').set('sidebarDesc', 'Klicken Sie auf ein Wort, um dessen Bedeutung anzuzeigen');
ReadingSpaceMap.get('fr').set('sidebarDesc', 'Cliquez sur un mot pour afficher sa signification');
ReadingSpaceMap.get('sr').set('sidebarDesc', 'Кликните на реч да бисте показали њено значење');

// Dugme za oznacavanje reci poznatom
ReadingSpaceMap.get('en').set('knowWordButton', 'I know this word.');
ReadingSpaceMap.get('de').set('knowWordButton', 'Ich kenne dieses Wort.');
ReadingSpaceMap.get('fr').set('knowWordButton', 'Je connais ce mot.');
ReadingSpaceMap.get('sr').set('knowWordButton', 'Знам ову реч.');

// Placeholder u polju za dodavanje korisnickog prevoda
ReadingSpaceMap.get('en').set('addTransPlaceholder', 'your translation');
ReadingSpaceMap.get('de').set('addTransPlaceholder', 'Ihre Übersetzung');
ReadingSpaceMap.get('fr').set('addTransPlaceholder', 'votre traduction');
ReadingSpaceMap.get('sr').set('addTransPlaceholder', 'Ваш превод');

// Submit dugme za polje opisano iznad
ReadingSpaceMap.get('en').set('addTransButton', 'add');
ReadingSpaceMap.get('de').set('addTransButton', 'hinzufügen');
ReadingSpaceMap.get('fr').set('addTransButton', 'ajouter');
ReadingSpaceMap.get('sr').set('addTransButton', 'додај');

// Natpis nad linkovima do drugih recnika
ReadingSpaceMap.get('en').set('otherTransHeading', 'See other translations');
ReadingSpaceMap.get('de').set('otherTransHeading', 'Weitere Übersetzungen');
ReadingSpaceMap.get('fr').set('otherTransHeading', `Voir d'autres traductions`);
ReadingSpaceMap.get('sr').set('otherTransHeading', 'Погледај друге преводе');

// Ispis za animaciju dok se ucitava stranica
ReadingSpaceMap.get('en').set('loadingPageDesc', 'Loading page');
ReadingSpaceMap.get('de').set('loadingPageDesc', 'Seite wird geladen');
ReadingSpaceMap.get('fr').set('loadingPageDesc', 'Chargement du page');
ReadingSpaceMap.get('sr').set('loadingPageDesc', 'Учитавам страницу');

// Lession ispis za natpis nad progress barom
ReadingSpaceMap.get('en').set('lessonName', 'Lesson');
ReadingSpaceMap.get('de').set('lessonName', 'Lektion');
ReadingSpaceMap.get('fr').set('lessonName', 'Leçon');
ReadingSpaceMap.get('sr').set('lessonName', 'Лекција');

// Link za povratak na listu lekcija
ReadingSpaceMap.get('en').set('lessonListLink', 'all lessons');
ReadingSpaceMap.get('de').set('lessonListLink', 'аlle Lektionen');
ReadingSpaceMap.get('fr').set('lessonListLink', 'toutes les leçons');
ReadingSpaceMap.get('sr').set('lessonListLink', 'све лекције');

ReadingSpaceMap.get('en').set('context', 'Context of word in ');
ReadingSpaceMap.get('de').set('context', 'Kontext des Wortes in ');
ReadingSpaceMap.get('fr').set('context', 'Contexte du mot dans ');
ReadingSpaceMap.get('sr').set('context', 'Контекст речи у ');

export { LoginPageMap, CoursesPageMap, ReadingSpaceMap };
