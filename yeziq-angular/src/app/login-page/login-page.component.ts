import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Subscription } from 'rxjs';
import { Router, NavigationEnd, RouterEvent } from '@angular/router';
import { User } from '../models/user.model';
import { LoginPageMap } from '../models/langmaps';
import { FlagLinks, SrcLanguages, TargetLanguages, LangNames } from '../models/supported-languages';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {

  // Nizovi sa oznakama izvornih i ciljnih jezika
  public srcLanguages = SrcLanguages;
  public targetLanguages = TargetLanguages;

  // Mapa sa putanjama do zastavica
  public flagLinksMap = FlagLinks;

  // Mapa sa nazivima jezika u zavisnosti od trenutno izabranog izvornog jezika
  public langNames = LangNames;
  // Trenutno izabran izvorni jezik
  public srcLang: string;
  // Mapa sa natpisima za prikaz u svim jezicima
  public langMap = LoginPageMap;

  // Formulari za prijavu i registraciju
  public loginForm: FormGroup;
  public registerForm: FormGroup;

  // Toggle za prikaz gresaka kod oba formulara
  public showErrors = false;
  // Eventualna poruka o gresci sa servera
  public serverMsg = '';

  // Prvo se prikazuje login formular, pogledati HTML sablon
  public showLoginForm: boolean = true;

  private activeSubs: Subscription[] = [];

  constructor(private formBuilder: FormBuilder,
    private userService: UserService, private router: Router) {

    // Ako je korisnik vec prijavljen a preusmeren je sa 'pogresnog' linka
    const getUser = this.userService.getUser();
    if (getUser) {
      router.navigate(['/courses']);
    }

    // Browser se pita za jezik, ako nije podrzan, postavlja se na engleski
    this.srcLang = navigator.language.substr(0, 2);
    if (!this.srcLanguages.includes(this.srcLang))
      this.srcLang = 'en';

    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

    this.registerForm = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(4), Validators.maxLength(25)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(25)]],
      confirmPassword: ['', [Validators.required, this.confirmPasswordValidator()]],
      email: ['', [Validators.required, Validators.email]],
      targetLang: [this.srcLang === 'en' ? 'de' : 'en']
    });
  }

  // U registerForm password i confirmPassword moraju imati jednake vrednosti
  private confirmPasswordValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      if (!this.registerForm)
        return null;
      const passwordConfirmed = control.value === '' ? false : control.value === this.registerForm.get('password').value;
      return passwordConfirmed ? null : { unconfirmedPassword: true };
    }
  }

  // Menjanje izvornog jezika preko dropup menija sa zastavicama
  public changeSrcLang(lang: string): void {
    this.srcLang = lang;
    this.registerForm.get('targetLang').setValue(this.srcLang === 'en' ? 'de' : 'en');
  }

  // Ako je validan login, ide se na stranu za kurseve
  public submitLoginInfo(data): void {
    this.serverMsg = '';

    if (!this.loginForm.valid) {
      this.showErrors = true;

      return;
    }

    this.showErrors = false;

    const sub = this.userService.login(data).subscribe((user: User) => {
      if (user)
        this.router.navigate(['/courses']);
      else {
        this.serverMsg = this.langMap.get(this.srcLang).get('incorrectUserPass');
      }
    });

    this.activeSubs.push(sub);
  }

  // Registracija novog korisnika i automatski login
  public submitRegisterInfo(form): void {
    this.serverMsg = '';

    if (!this.registerForm.valid) {
      this.showErrors = true;

      return;
    }

    this.showErrors = false;

    const data = {
      ...form,
      srcLang: this.srcLang
    };

    const registerSub = this.userService.createUser(data).subscribe((user: User) => {
      if (user) {

        const loginSub = this.userService.login({ username: data.username, password: data.password }).subscribe((obj: any) => {
          if (user)
            this.router.navigate(['/courses']);
        });

        this.activeSubs.push(loginSub);

      } else {
        this.serverMsg = this.langMap.get(this.srcLang).get('alreadyExists');
      }
    });
    this.activeSubs.push(registerSub);
    this.registerForm.reset();
    this.registerForm.get('targetLang').setValue(this.srcLang === 'en' ? 'de' : 'en');
  }

  // Login / Register formulari toggle 
  public toggleForms(): void {
    this.showLoginForm = !this.showLoginForm;
    this.serverMsg = '';
    this.showErrors = false;
  }

  public get usernameLogin() {
    return this.loginForm.get('username');
  }

  public get passwordLogin() {
    return this.loginForm.get('password');
  }

  public get username() {
    return this.registerForm.get('username');
  }

  public get password() {
    return this.registerForm.get('password');
  }

  public get confirmPassword() {
    return this.registerForm.get('confirmPassword');
  }

  public get email() {
    return this.registerForm.get('email');
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.activeSubs.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }
}
