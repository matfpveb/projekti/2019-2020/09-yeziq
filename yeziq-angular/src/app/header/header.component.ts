import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../models/user.model';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { WordsService } from '../services/words.service';
import { CoursesPageMap } from '../models/langmaps';
import { LangNames, FlagLinks, NavbarFlagLinks, TargetLanguages } from '../models/supported-languages';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input()
  public user: BehaviorSubject<User>;
  @Input()
  public knownWordCount: BehaviorSubject<number>;
  @Input()
  public level: BehaviorSubject<number>;

  // Emituje se courses ili reading-space komponenti kada se promeni ciljni jezik
  @Output('newTargetLang')
  public emitTargetLang: EventEmitter<string> = new EventEmitter<string>();

  // Mape za natpise, putanje do zastavica, imena jezika
  public langMap = CoursesPageMap;
  public langNames = LangNames;
  public languages = TargetLanguages;
  public flagLinksMap= FlagLinks;
  public navbarFlagsMap = NavbarFlagLinks;

  constructor(private router: Router, private userServices: UserService) {
  };

  // Pri logoutu se u servisu userServices promenljiva user postavlja na null i ide se na stranu za login
  public logout(): void {
    this.userServices.resetUser();
    this.router.navigate(['']);
  }


  // Salju se informacije u promenjenom ciljnom jeziku roditeljskoj komponenti (courses ili reading-space)
  public changeTargetLang(lang: string): void {
    this.emitTargetLang.emit(lang);
  }

  public getLevelTooltip(srcLang: string): string {

    let remaining: number;
    const lvl = this.level.getValue();
    const known = this.knownWordCount.getValue()

    switch(lvl) {
      case 0:
        remaining = 200 - known;
        break;
      case 1:
        remaining = 500 - known;
        break;
      case 2:
        remaining = 1250 - known;
      default:
        remaining = Math.ceil(known/2500)*2500 - known;
        break;
    }

    return `${remaining} ` + this.langMap.get(srcLang).get('levelTooltip') + ` ${lvl + 1}`;
  }

  ngOnInit() {
  }

}
