import { Component, OnInit, OnDestroy } from '@angular/core';
import { Translations } from '../models/translations.model';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Course } from '../models/course.model';
import { Subscription, Observable, BehaviorSubject } from 'rxjs';
import { UserService } from '../services/user.service';
import { WordsService } from '../services/words.service';
import { User } from '../models/user.model';
import { Word } from '../models/word.model';
import { ReadingSpaceMap } from '../models/langmaps';
import { RegexMap, DictSupportedLangMap, DictionaryQueriesMap } from '../models/supported-languages';
import { parse } from 'querystring';

@Component({
  selector: 'app-reading-space',
  templateUrl: './reading-space.component.html',
  styleUrls: ['./reading-space.component.css']
})
export class ReadingSpaceComponent implements OnInit, OnDestroy {

  // Mapa sa regularnim izrazima, koristi je main komponenta za prikaz teksta
  private regexMap = RegexMap;

  // Mapa sa natpisima u odgovarajucem jeziku
  public langMap = ReadingSpaceMap;

  public user: User;
  public knownWordCount = new BehaviorSubject<number>(-1);
  public level = new BehaviorSubject<number>(-1);

  // Kurs koji je izabran
  public course: Course;
  // Niz tokena za trenutnu stranicu trenutne lekcije.
  // Koristi ga HTML sablon.
  public currPage: Array<string>;
  // Indeks stranice u okviru lekcije koja se prikazuje
  private currPageInd: number;
  // Indeks trenutne lekcije u okviru kursa
  public currLessonInd: number;

  // Side komponenta prikazuje informacije o poslednjoj reci
  // na koju je kliknuo korisnik. Na pocetku nema prikaza.
  public selectedWord: string = null;
  public sideHidden: boolean = true;

  // Informacije u poznatim ("belim") i yeziq ("zutim") recima iz baze
  public knownDB = new Map<string, Translations>();
  public yeziqDB = new Map<string, Translations>();

  // Sve validne reci u okviru lekcije
  public currWords = new Map<string, Translations>();

  // Naziv recnika i oblik upita za side komponentu, otvarace se novi prozori
  public dictionaries: Map<string, string>;

  private activeSubs: Subscription[] = [];

  public context: string[];

  constructor(private wordsService: WordsService,
    private userService: UserService,
    private routes: ActivatedRoute,
    private router: Router) {

    // Ako se direktno pristupa /read linku bez logina, vraca se na login
    const getUser = this.userService.getUser();
    if (!getUser)
      router.navigate(['/']);

    // Ako ne postoje odgovarajuci parametri u url-u
    this.routes.paramMap.subscribe(params => {
      if (!params.get('course') || !params.get('lesson')) {
        router.navigate(['/courses']);
      }
    });

    const userSub = getUser.subscribe((user: User) => {
      // Ucitava se korisnik
      this.user = user;

      // Inicijalizuje se mapa recnika sa oblicima upita onima koji podrzavaju i srcLang i targetLang
      this.dictionaries = new Map<string, string>();

      const dictSupportedLang = DictSupportedLangMap;
      const dictQueriesMap = DictionaryQueriesMap;

      dictSupportedLang.forEach((languages, dictName) => {
        if (!languages.includes(this.user.srcLang) || !languages.includes(this.user.targetLang))
          return;

        this.dictionaries.set(dictName, dictQueriesMap.get(dictName));
      });

      // Azurira se broj poznatih reci u trenutnom ciljnom jeziku
      const knownWordCountSub = this.wordsService.getKnownWordCount({ userId: this.user._id, targetLang: this.user.targetLang })
        .subscribe((count: number) => {

          this.knownWordCount.next(count);
          this.level.next(this.getLevel(count));
        });
      this.activeSubs.push(knownWordCountSub);

      // Informacije o trenutno zapamcenim recima iz baze, azuriraju se mape knownDB i yeziqDB
      const wordsSub = this.wordsService.getWords({ id: this.user._id, targetLang: this.user.targetLang }).subscribe((words: Word[]) => {
        words.forEach((word: Word) => {

          if (word.status === 1)
            this.yeziqDB.set(word.word, new Translations('status1', word.chosenTrans, word.possibleTrans, word.userTrans));
          else
            this.knownDB.set(word.word, new Translations('', word.chosenTrans, word.possibleTrans, word.userTrans));
        });

        // Ucitava se odgovarajuca lekcija odgovarajuceg kursa iz url
        let courseId: string;
        this.routes.paramMap.subscribe(params => {

          courseId = params.get('course');
          this.currLessonInd = parseInt(params.get('lesson')) - 1;

          // Trazi se i ucitava kurs i trazena lekcija
          const courseSub = this.userService.getCourses().subscribe((courses: Course[]) => {

            // Provera da li su parametri validni, tj. da li su dobijeni klikom na lekciju.
            // Ako ne, znaci da su uneti preko url-a, ide se na courses, a odatle eventualno na login ako korisnik nije prijavljen
            const filteredCourses = courses.filter(course => course._id === courseId);

            if (filteredCourses.length === 0 || this.currLessonInd < 0 || this.currLessonInd >= filteredCourses[0].lessons.length) {

              router.navigate(['/courses']);
            } else {

              this.course = filteredCourses[0];
              this.loadCourse(this.course);
            }
          });

          this.activeSubs.push(courseSub);
        });
      });

      this.activeSubs.push(wordsSub);
    });

    this.activeSubs.push(userSub);
  }

  // Racuna nivo korisnika u trenutnom jeziku na osnovu broja poznatih reci
  private getLevel(known: number): number {
    let level: number;

    if (known < 200) level = 0;
    else if (known < 500) level = 1;
    else if (known < 1250) level = 2;
    else level = Math.floor(2 + known / 2500); // na svakih 2500 (2500 -> 3, 5000 -> 4, ...)

    return level;
  }

  public onNewTargetLang(newTargetLang: string): void {
    const changeUserSub = this.userService.changeTargetLang({ id: this.user._id, newTargetLang: newTargetLang }).subscribe((user: User) => {
      // Samo se ide nazad na kurseve cim se azurira ciljni jezik
      this.router.navigate(['/courses']);
    });

    this.activeSubs.push(changeUserSub);
  }

  // Kada korisnik klikne na rec koju je procitao
  onSelectedWord(words: string[]): void {
    this.selectedWord = words.slice(-1)[0];
    this.context = words.slice(0, -1);
    this.sideHidden = false;

    // Dodatno, na malim uredjajima je sidebar mozda skriven pa se prikazuje
    document.getElementById('side').style.display = 'inline-block';
  }

  /**
  *  Klasifikuje validne reci iz kursa i postavlja im statuse.
  *  Te niske 'status0' (nepoznata, "plava" rec) ili 'status1' (yeziq)
  *  su takodje CSS klase koje odredjuju prikaz tih tokena u tekstu.
  */
  private loadCourse(course: Course): void {

    const regex = new RegExp('^' + this.regexMap.get(this.user.targetLang) + '$');
    course.lessons.forEach(lesson => lesson.forEach(page => page.filter(token => regex.test(token)).forEach(word => {

      // Obrada jedne reci se vrsi samo jednom
      word = word.toLowerCase();
      if (this.currWords.has(word))
        return;

      // U currWords ce se naci sve validne reci
      this.currWords.set(word, new Translations('', '', [], ''));

      // U knownDB samo ako su poznate, u currWords se rec azurira informacijama iz baze
      if (this.knownDB.has(word)) {
        this.currWords.get(word).chosenTrans = this.knownDB.get(word).chosenTrans;
        this.currWords.get(word).possibleTrans = this.knownDB.get(word).possibleTrans.slice();
        this.currWords.get(word).userTrans = this.knownDB.get(word).userTrans;
      }
      else if (this.yeziqDB.has(word)) {
        // Slicno ako je rec yeziq
        this.currWords.get(word).status = 'status1';
        this.currWords.get(word).chosenTrans = this.yeziqDB.get(word).chosenTrans;
        this.currWords.get(word).possibleTrans = this.yeziqDB.get(word).possibleTrans.slice();
        this.currWords.get(word).userTrans = this.yeziqDB.get(word).userTrans;
      }
      else {
        // Slucaj ako rec nije ni poznata ni jezik, tj. nepoznata je.
        // Lista mogucih prevoda je prazna, i izabran prevod je prazna niska.
        // Te vrednosti se azuriraju kada korisnik klikne na doticnu rec i aplikacija
        // dobije informacije od recnika.
        this.currWords.get(word).status = 'status0';
        this.currWords.get(word).possibleTrans = [];
        this.currWords.get(word).userTrans = '';
      }
    })));

    // Izabrana lekcija se uvek cita od prve stranice.
    this.currPageInd = 0;
    // Azurira se currPage niz tokena odgovarajucim nizom koji predstavlja trenutnu stranicu.
    this.currPage = course.lessons[this.currLessonInd][this.currPageInd];
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.activeSubs.forEach(sub => {
      sub.unsubscribe();
    });
  }

}
